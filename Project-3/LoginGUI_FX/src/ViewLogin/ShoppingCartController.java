package ViewLogin;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

public class ShoppingCartController {
	ObservableList<Cart> data = FXCollections.observableArrayList();

	@FXML
	TableView<Cart> table;

	@FXML
	private TableColumn<?, ?> authorCol;

	@FXML
	private TableColumn<?, ?> bookCol;

	@FXML
	private Label isbnLabel;

	@FXML
	private Label bookNameLabel;

	@FXML
	private Label authorLabel;

	@FXML
	private Label editionLabel;

	@FXML
	private Label yearLabel;

	@FXML
	private Label priceLabel;

	private String username;

	public ShoppingCartController() {
		
	}
	public void backButton(ActionEvent event) throws IOException {
		Parent Login = FXMLLoader.load(getClass().getResource("Bookstore.fxml"));
		Scene LoginScene = new Scene(Login);
		Stage window2 = (Stage) ((Node) (event.getSource())).getScene().getWindow();
		window2.setScene(LoginScene);
		window2.show();
	}
	@FXML
	public void CheckoutButton(ActionEvent event) throws IOException {
		Parent Login1 = FXMLLoader.load(getClass().getResource("CreditCardTransaction.fxml"));
		Scene LoginScene1 = new Scene(Login1);
		Stage window3 = (Stage) ((Node) (event.getSource())).getScene().getWindow();
		window3.setScene(LoginScene1);
		window3.show();
	}
	public void sendData(String username) {
		this.username = username;
		System.out.println("VERIFY " + username);

	}
}
